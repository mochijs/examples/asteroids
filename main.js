// This grabs all the code that helps the game work
requirejs(['../engine/Mochi', './shipcontrol', './bullet', './asteroid', './bounds', 'endgamestate', 'score'],
function(Mochi, ShipControl, Bullet, Asteroid, Bounds, EndGameState, Score) {


    class PlayerFactory {
        constructor(main) {
            this.ship = new Mochi.Content.Asset("assets/ship_internet.png");
            this.main = main;
        }

        getAssets() {
            return [this.ship];
        }

        make(x, y, input, bulletFactory, width, height) {
            let player = new Mochi.Entity(x, y);
            player.add(new Mochi.Graphics.Sprite(this.ship, { width: 1, height: 1.5, anchor: { x: .5, y: .5, rotation: Mochi.Math.degreesToRadians(90) } }));
            // add a body so that a player now has collision
            player.add(new Mochi.Physics.Body(new Mochi.Physics.Shape.Circle(.75)));
            // Player's have a special control component that directs their location/body.
            player.add(new ShipControl(input, bulletFactory, width, height));
            player.add(new Bounds.Handler(width, height));
            player.add(new Bounds.PositionResolve());
            player.events.subscribe("shoot", function(bullet) {
                this.main.scene.addEntity(bullet);
            }.bind(this));
            // if the player collides with an asteroid, game over.
            player.events.subscribe("collision", function(entity) {
                if (!entity.has("asteroid"))
                    return;
                this.main.game.swapState("end");
            }.bind(this));
            player.init();
            return player;
        }
    }

    class BulletFactory {
        constructor(main) {
            this.bullet = new Mochi.Content.Asset("assets/This_Works!.png");
            this.main = main;
        }

        getAssets() {
            return [this.bullet];
        }

        make(x, y, rotation, width, height) {
            let bullet = new Mochi.Entity(x, y);
            bullet.add(new Mochi.Graphics.Sprite(this.bullet, {width: 3, height: 1, anchor: { x: .5, y: .5 } }));
            bullet.add(new Mochi.Physics.Body(new Mochi.Physics.Shape.Box(3, 1)));
            bullet.add(new Bullet(rotation));
            bullet.add(new Bounds.Handler(width, height));
            bullet.add(new Bounds.DeathResolve(this.main.scene));
            // if a bullet collides with a asteroid, it'll destroy it.
            bullet.events.subscribe("collision", function(entity) {
                if (!entity.has("asteroid"))
                    return;
                console.log("Hit an asteroid!");
                this.main.scene.removeEntity(entity);
                this.main.score.value += 10;
            }.bind(this));
            bullet.init();
            return bullet;
        }
    }

    /**
     * The main class that runs the game.
     */
    class AsteroidGame {
        constructor() {
            let screenWidth = window.innerWidth * .98;
            this.us = new Mochi.Util.UnitSystem(AsteroidGame.Units, screenWidth);

            this.game = new Mochi.Game({
                width: this.us.pixelsToUnits(screenWidth),
                height: this.us.pixelsToUnits(window.innerHeight * .98),
                unitSystem: this.us,
                fps: 60,
                startState: "load"
            });

            this.asteroidFactory = new Asteroid.Factory(AsteroidGame.SpawnRate, this);
            this.playerFactory = new PlayerFactory(this);
            this.bulletFactory = new BulletFactory(this);

            // Score for the game.
            this.score = new Score(0, this.game.getPixelHeight() - Score.HEIGHT);

            // Load state that transitions to intro state when everything is loaded.
            this.game.addState(
                new Mochi.GameStates.LoadState("load", this.getAssets(), "intro")
            );
            // the main game state.
            this.game.addState(
                new Mochi.GameState("main").setup(this.update.bind(this), this.draw.bind(this), function() {}, this.enter.bind(this))
            );
            // Set the current state to be the basic intro state
            this.game.addState(
                new Mochi.GameStates.IntroState("intro", ["Enter", " "], "main").setGameName("Asteroids")
            );
            this.game.addState(
                new EndGameState("end", this.score)
            );
        }

        /** Returns all assets needed to be loaded by the game */
        getAssets() {
            return this.asteroidFactory.getAssets()
                .concat(this.playerFactory.getAssets())
                .concat(this.bulletFactory.getAssets());
        }

        start() {
            this.game.start();
        };

        /**
         * Called when the main game state is entered.
         */
        enter() {
            this.score.value = 0;
            // set up our systems
            this.drawManager = new Mochi.Graphics.UnitDrawManager(this.us);
            this.collisionManager = new Mochi.Physics.CollisionManager(0, 0, this.game.width, this.game.height, 1, 1);
            this.emitterManager = new Mochi.Physics.EmitterManager();
            this.scene = new Mochi.Scene();
            this.scene.addManager(this.drawManager).addManager(this.collisionManager).addManager(this.emitterManager).init();

            // add some entities for testing
            this.player = this.playerFactory.make(4, 4, this.game.input, this.bulletFactory, this.game.width, this.game.height);
            this.spawner = Asteroid.MakeSpawner(this.game, this.asteroidFactory, AsteroidGame.SpawnRate);
            this.scene.addEntity(this.player);
            this.scene.addEntity(this.spawner);
        }

        /**
         * Renders the game
         * @param ctx The context needed.
         */
        draw(ctx) {
            ctx.save();
            // TODO integrate into draw
            this.drawManager.clear(ctx, this.game);
            this.scene.draw(ctx);
            this.score.draw(ctx);
            ctx.restore();
        }

        /** The update loop */

        update(ms) {
            this.player.shipControl.update(ms/1000);
            this.scene.update(ms);
        }
    }
    AsteroidGame.Units = 32;
    AsteroidGame.SpawnRate = 2000;

    var asteroid = new AsteroidGame();
    // Initialize and begin the game.
    asteroid.start();
    console.log(asteroid);
});

