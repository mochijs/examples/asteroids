define(['../engine/Mochi'], function(Mochi) {

    /**
     * A component that throws event calls when an object wades out of bounds.
     * Requires Body component.
     */
    class BoundsHandler extends Mochi.Component {
        /**
         * Sets up the bounds of the world to alert on
         */
        constructor(width, height) {
            super();
            this.width = width;
            this.height = height;
        }

        init (entity) {
            this.entity = entity;
            // subscribe on update to check bounds
            this.entity.events.subscribe("update", this.checkBounds.bind(this));
        }

        checkBounds (data) {
            let box = this.entity.body.getBoundingBox();
            let event = {
                entity: this.entity
            };
            if (box.x > this.width) {
                event.right = true;
            } else if (box.x + box.width < 0) {
                event.right = false;
            }
            if (box.y > this.height) {
                event.top = true;
            } else if (box.y + box.height < 0) {
                event.top = false;
            }

            if ('right' in event || 'top' in event) {
                this.entity.events.broadcast("OutOfBounds", event);
            }
        }
    }
    BoundsHandler.Name = "boundsHandler";

    /** Resolves bounds through killing */
    class DeathResolve extends Mochi.Component {
        /**
         * create a component that resolves bounds throws with death
         * @param scene The scene to remove the entity from
         */
        constructor(scene) {
            super();
            this.scene = scene;
        }

        init(entity) {
            entity.events.subscribe("OutOfBounds", this._resolve.bind(this));
        }

        _resolve(data) {
            console.log("Resolved by death, killing ", data.entity);
            this.scene.removeEntity(data.entity);
        }
    }
    DeathResolve.Name = "deathResolve";

    /** Resolves bounds through position adjustment */
    class PositionResolve extends Mochi.Component {
        constructor() {
            super();
        }

        init(entity) {
            this.handler = entity.boundsHandler;
            entity.events.subscribe("OutOfBounds", this._resolve.bind(this));
        }

        _resolve(data) {
            let entity = data.entity;
            let box = entity.body.getBoundingBox();
            console.log("Resolved by teleport, moving from " + entity.x + ", " + entity.y);
            // resolve the positions
            if ('right' in data) {
                if (data.right)
                    entity.x -= this.handler.width - box.width / 2;
                else
                    entity.x += this.handler.width - box.width / 2;
            }
            if ('top' in data) {
                if (data.top)
                    entity.y -= this.handler.height - box.height / 2;
                else
                    entity.y += this.handler.height - box.height / 2;
            }
        }
    }
    PositionResolve.Name = "positionResolve";

    return {
        Handler: BoundsHandler,
        DeathResolve: DeathResolve,
        PositionResolve: PositionResolve
    };
});