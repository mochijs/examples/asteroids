# Team:
 1. Kenny Jones (kgjones3)
 2. Justin Patzer (jepatzer)
 3. Robby Seligson (rfseligs)

# For the Professor:

Everything is packaged inside. Just open up index.html, which uses requirejs (see libraries in Engine readme) to
load main.js. 

# Checking out project
This project contains a 'submodule' (a git repository in a git repository).

Instead of just cloning it directly, clone it recursively.

```git
git clone -b {yourbranch} --recursive git@gitlab.com:csc481/asteroids.git
```

If you already downloaded the repo, you can type.

```git
git submodule init
git submodule update
```

It'll pull the game engine in.

## Pulling from local repo.
Make sure to pull recursively, or you can...
```bash
git pull
cd engine
git pull
```
