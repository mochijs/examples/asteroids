define(['../engine/Mochi'], function(Mochi) {
    class Bullet extends Mochi.Component{

        constructor(rotation){
            super();
            this.rotation = rotation;
            this.speed = 100;

        }

        init(entity) {
            this.entity = entity;

            this.entity.rotation = this.rotation;
            this.entity.body.speedX = this.speed * Math.cos(this.rotation);
            this.entity.body.speedY = this.speed * Math.sin(this.rotation);
        }

    }

    Bullet.Name = "bullet";
    return Bullet;
});