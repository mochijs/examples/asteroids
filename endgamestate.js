define(['../engine/Mochi'], function(Mochi) {

    class EndGameState extends Mochi.GameState {
        constructor(name, score) {
            super(name);
            this.score = score;
        }

        init(game) {
            this.game = game;
            this.game.input.onKeyPress("Enter", this.restart.bind(this));
        }

        restart() {
            this.game.swapState("intro");
        }

        /**
         * Draws the end game screen.
         */
        draw(ctx) {
            ctx.save();
            ctx.clearRect(0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());
            ctx.fillStyle = "gray";
            ctx.fillRect(0, 0, this.game.getPixelWidth(), this.game.getPixelHeight());
            // draw the score, and if they have a high score, show it.
            this.score.draw(ctx);
            if (this.score.save()) {
                this.score.drawHighScore(ctx, this.game.getPixelWidth() / 2, this.game.getPixelHeight() / 2);
            } else {
                this.score.drawGameOver(ctx, this.game.getPixelWidth() / 2, this.game.getPixelHeight() / 2);
            }
            ctx.restore();
        }
    }

    return EndGameState;
});